<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // $this->call(UsersTableSeeder::class);
        $this->call(Create_Data_About::class);
        $this->call(Create_Data_Expirience::class);
        $this->call(Create_Data_Education::class);
        $this->call(Create_Data_Skills::class);
        $this->call(Create_Data_Interest::class);
        $this->call(Create_Data_Pages::class);
        $this->call(Create_Data_Social::class);






    }
}
