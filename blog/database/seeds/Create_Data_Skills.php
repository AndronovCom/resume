<?php

use Illuminate\Database\Seeder;
use App\Models\SkillsModel;
use Illuminate\Database\Schema\Blueprint;

class Create_Data_Skills extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        if(Schema::hasTable('skills') && SkillsModel::count()==0)
        {
            SkillsModel::insert([
                ['description'=>'Языки программирования и технологии','title' => 'C'],
                ['description'=>null,'title' => 'C++'],
                ['description'=>null,'title' => 'WinApi'],
                ['description'=>null,'title' => 'HTML'],
                ['description'=>null,'title' => 'CSS'],
                ['description'=>null,'title' => 'JS'],
                ['description'=>null,'title' => 'C#'],
                ['description'=>null,'title' => 'MySQL'],
                ['description'=>null,'title' => 'PHP'],
                ['description'=>null,'title' => 'JQuery'],
                ['description'=>null,'title' => 'Laravel'],
                ['description'=>null,'title' => 'WordPress'],
                ['description'=>null,'title' => 'Git'],
            ]);
        }
    }
}
