<?php

use Illuminate\Database\Seeder;
use App\Models\EducationModel;
use Illuminate\Database\Schema\Blueprint;

class Create_Data_Education extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        if(Schema::hasTable('education') && EducationModel::count()==0)
        {
            EducationModel::insert([
                [
                    'title'         => 'ХАРЬКОВСКИЙ НАЦИОНАЛЬНЫЙ УНИВЕРСИТЕТ ВНУТРЕННИХ ДЕЛ (ХНУВД)',
                    'description'   => 'Образование высшее юридическое, специальность юрист-правовед,
                                        имею специальное звание старшего лейтенанта',
                    'date'          =>  'август 2003 -июнь 2007'
                ],
                [
                    'title'         => 'Компьютерная академия "ШАГ"',
                    'description'   => 'В мае 2017 года я поступил на курсы разработки ПО. До начала 
                                         обучения о программировании я не знал ничего, однако мне хотелось реализовать себя в данной области. Поэтому процесс обучения проходил для меня с большим энтузиазмом. 
                                         В академии я с удовольствием работал над лабараторными работами. Промежуточным результатом считаю высокие баллы на экзаменах.',
                    'date'          =>  'май 2017 - сентябрь 2018'
                ]
            ]);
        }
    }
}
