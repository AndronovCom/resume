<?php

use Illuminate\Database\Seeder;
use App\Models\SocialModel;

class Create_Data_Social extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        if(Schema::hasTable('socials') && SocialModel::count()==0)
        {
            SocialModel::insert([
               [
                    'name'      =>'bitbucket',
                    'social'    =>'https://bitbucket.org/AndronovCom/resume/src/master/',
                    'image'     =>'fab fa-bitbucket'
               ],
                [
                    'name'      =>'facebook',
                    'social'      =>null,
                    'image'     =>'fab fa-facebook-f'
                ]
            ]);
        }
    }
}
