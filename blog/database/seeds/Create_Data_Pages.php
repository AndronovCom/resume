<?php

use Illuminate\Database\Seeder;
use App\Models\PagesModel;

class Create_Data_Pages extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        if(Schema::hasTable('pages') && PagesModel::count()==0)
        {
            PagesModel::insert([

                [
                    'name'    => 'обо мне',
                    'aliase'  => 'about'

                ],
                [
                    'name'    => 'опыт',
                    'aliase'  => 'experiences'

                ],
                [
                    'name'    => 'образование',
                    'aliase'  => 'educations'

                ],
                [
                    'name'    => 'навыки',
                    'aliase'  => 'skills'

                ],
                [
                    'name'    => 'интересы',
                    'aliase'  => 'interest'

                ],
            ]);
        }
    }
}
