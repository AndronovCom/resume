<?php

use Illuminate\Database\Seeder;
use App\Models\AboutModel;

class Create_Data_About extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        if(AboutModel::count()==0)
        {
           AboutModel::create([
                'name'          => 'Сергей',
                'surname'       => 'Андронов',
                'country'       => 'Украина',
                'city'          => 'Харьков',
                'phone'         => '+380634486767',
                'email'         => 'sab2rih@gmail.com',
                'image'         => 'profile.jpg',
                'description'   => '<p>Женат, двое детей. Целеустремленный и мотивированный.</p> 
                                   <p>Данная страница была создана в процессе ознакомления с фрейворком Laravel.</p>',
           ]);
        }
    }
}
