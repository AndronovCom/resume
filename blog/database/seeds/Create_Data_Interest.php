<?php

use Illuminate\Database\Seeder;
use App\Models\InterestModel;
use Illuminate\Database\Schema\Blueprint;


class Create_Data_Interest extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        if(Schema::hasTable('interests') && InterestModel::count()==0)
        {
            InterestModel::insert([
                'description'   =>  'Люблю активный вид отдыха'
            ]);
        }
    }
}
