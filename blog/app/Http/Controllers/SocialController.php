<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class SocialController extends Controller
{
    private $access_token="https://graph.facebook.com/v3.2/me?fields=id%2Cname%2Cposts%7Bfull_picture%2Cdescription%2Cid%2Clink%2Ccreated_time%7D&access_token=EAAF0OUXLL9sBAFZCUfbb77icGoSq4XCZB0fZBfPRGKfBTxIrX4Dc2b4TqEoqmQOz5dIaJCHl2vCGJljEZCNECWuz2u9jQTHXZBnMrH4i51OS3DRznG6zYZAtVSBbYIQ5czgkZAqd0hfk9dvST3mrFvnN1wDWeMjLgCDUE1e5UCvIgZDZD";
    private $json;
    private $object;

    private function getJson()
    {
        $this->json=file_get_contents($this->access_token);
    }

    private function decodeJson()
    {
        $this->object=json_decode($this->json,true);
    }

    private function timeString()
    {
        foreach ($this->object['posts']['data'] as &$value)
        {
            $value['created_time']=date('d.m.Y',strtotime($value['created_time']));
        }
    }
    public function execute(Request $request)
    {
        $this->getJson();
        $this->decodeJson();
        $this->timeString();
     //dd($this->object['posts']['data']);
        return view('site/social',['posts'=>$this->object['posts']['data']]);

    }
}
