<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\AboutModel;
use App\Models\EducationModel;
use App\Models\ExperienceModel;
use App\Models\InterestModel;
use App\Models\PagesModel;
use App\Models\SkillsModel;
use App\Models\SocialModel;

class IndexController extends Controller
{
    public function execute()
    {
        $pages=PagesModel::get(['name','aliase']);
        $about=AboutModel::get(['name','surname','country','city','phone','email','image','description']);
        $experiences=ExperienceModel::get(['title','description','date']);
        $educations=EducationModel::get(['title','description','date']);
        $skills=SkillsModel::get(['description','title']);
        $socials=SocialModel::get(['name','social','image']);
        $interest=InterestModel::get(['description']);

        $menu=[];
        foreach ($pages as $page)
        {
            $item=['title'=>$page->name,'aliase'=>$page->aliase];
            $menu[]=$item;
        }

        return view('site/index',[
            'pages'             =>$pages,
            'menu'              =>$menu,
            'about'             =>$about,
            'experiences'       =>$experiences,
            'educations'        =>$educations,
            'skills'            =>$skills,
            'interest'          =>$interest,
            'socials'           =>$socials
        ]);
    }
}
