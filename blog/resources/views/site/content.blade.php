@if(isset($menu))
    @foreach($menu as $key=>$value)
            @if(isset(${$value['aliase']})&&$value['aliase']=='about')
            <section class="resume-section p-3 p-lg-5 d-flex d-column" id="{{$value['aliase']}}">
                <div class="my-auto">
                        <h1 class="mb-0">{{${$value['aliase']}[0]['surname']}}
                            <span class="text-primary">{{${$value['aliase']}[0]['name']}}</span>
                        </h1>
                        <div class="subheading mb-5">{{${$value['aliase']}[0]['country']}} · {{${$value['aliase']}[0]['city']}}
                            · {{${$value['aliase']}[0]['phone']}} ·
                            <a href="mailto:sab2rih@gmail.com">{{${$value['aliase']}[0]['email']}}</a>
                        </div>
                        <p class="lead mb-5">{!! ${$value['aliase']}[0]['description']!!}</p>
                        <div class="social-icons">

                            @if(isset($socials))
                                @foreach($socials as $social)
                                    @if(isset($social['social']))
                                    <a href="{{$social['social']}}">
                                        <i class="{{$social['image']}}"></i>
                                    </a>
                                    @else
                                        <a href="{{route('facebook')}}">
                                            <i class="{{$social['image']}}"></i>
                                        </a>
                                    @endif
                                @endforeach
                            @endif

                        </div>
                </div>
            </section>
            <hr class="m-0">

        @elseif(isset(${$value['aliase']})&&$value['aliase']=='skills')

                <section class="resume-section p-3 p-lg-5 d-flex flex-column" id="{{$value['aliase']}}">
                    <div class="my-auto">
                        <h2 class="mb-5">{{$value['title']}}</h2>
                        <div class="subheading mb-3">{{${$value['aliase']}[0]['description']}}</div>
                        <ul class="list-group">
                                @foreach(${$value['aliase']} as $key=>$skill)
                                    <li class="list-group-item list-group-item-dark">{{$skill->title}}</li>
                                @endforeach
                        </ul>
                    </div>
                </section>
                <hr class="m-0">

        @else

                <section class="resume-section p-3 p-lg-5 d-flex flex-column" id="{{$value['aliase']}}">
                    <div class="my-auto">
                        <h2 class="mb-5">{{$value['title']}}</h2>
                    @foreach(${$value['aliase']} as $key=>$experience)
                                <div class="resume-item ">
                                    <div class="resume-content mr-auto">
                                        <h3 class="mb-0">@if(isset($experience->title)){{$experience->title}}@endif</h3>
                                        <div class="subheading mb-3"></div><p>@if(isset($experience->description)){{$experience->description}}@endif</p>
                                    </div>
                                    <div class="resume-date text-md-right">
                                        <span class="text-primary">@if(isset($experience->date)){{$experience->date}}@endif</span>
                                    </div>
                                </div>
                            @endforeach
                    </div>
                </section>
                <hr class="m-0">
            @endif
    @endforeach
@endif









