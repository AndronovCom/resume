<a class="navbar-brand js-scroll-trigger" href="#page-top">
    <span class="d-none d-lg-block">
        @if(isset($about))
        {!! Html::image('assets/img/'.$about[0]->image,'Andronov',['class'=>'img-fluid img-profile rounded-circle mx-auto mb-2']) !!}
        @endif
        </span>
</a>
<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
</button>
@if(isset($menu))
    <div class="collapse navbar-collapse" id="navbarSupportedContent">
        <ul class="navbar-nav">
            @foreach($menu as $item)
            <li class="nav-item">
                <a class="nav-link js-scroll-trigger" href="{{'#'.$item['aliase']}}">{{$item['title']}}</a>
            </li>
            @endforeach
        </ul>
    </div>
@endif
