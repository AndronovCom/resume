    @if(isset($posts))
        @foreach($posts as $post)
            @if(isset($post['full_picture']))
            <div class="container">
                <div class="card-group container" style="width: 22rem" >
                <div class="card">
                    <div class="card-body">

                            <img class="card-img-top" src="{{$post['full_picture']}}" alt="Card image cap">
                        @if(isset($post['description']))
                            <p class="card-text">{{$post['description']}}</p>
                        @endif
                        @if(isset($post['created_time']))
                            <p class="card-text"><small class="text-muted">{{$post['created_time']}}</small></p>
                        @endif
                            @endif
                    </div></div>
                </div>
            </div>
            @endforeach
            @endif
