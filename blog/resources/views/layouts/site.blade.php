<!DOCTYPE html>
<html lang="{{app()->getLocale()}}">

<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="CV Andronov">
    <meta name="author" content="Andronov">
    <meta name="keywords" content="cv rezume php laravel trainee junior">

    <!-- Facebook and Twitter intagration-->
    <meta property="og:type" content="website">
    <meta property="og:title" content="Andronov CV">
    <meta property="og:url" content="http://andronov-sergey/">
    <meta property="og:site_name" content="Andronov-Sergey/">
    <meta property="og:description" content="CV">

    <title>{{config('app.name','Andronov Sergey')}}</title>

    <!-- Bootstrap core CSS -->
    <link href="{{asset('assets/vendor/bootstrap/css/bootstrap.min.css')}}" rel="stylesheet">

    <!-- Custom fonts for this template -->
    <link href="https://fonts.googleapis.com/css?family=Saira+Extra+Condensed:500,700" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Muli:400,400i,800,800i" rel="stylesheet">
    <link href="{{asset('assets/vendor/fontawesome-free/css/all.min.css')}}" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="{{asset('assets/css/resume.min.css')}}" rel="stylesheet">

</head>

<body id="page-top">
<script src="{{asset('assets/js/facebookSDK.js')}}"></script>


<nav class="navbar navbar-expand-lg navbar-dark bg-primary fixed-top" id="sideNav">
    @yield('header')
</nav>
<div class="container-fluid p-0">
    @yield('content')

</div>

<!-- Bootstrap core JavaScript -->
<script src="{{asset('assets/vendor/jquery/jquery.min.js')}}"></script>
<script src="{{asset('assets/vendor/bootstrap/js/bootstrap.bundle.min.js')}}"></script>

<!-- Plugin JavaScript -->
<script src="{{asset('assets/vendor/jquery-easing/jquery.easing.min.js')}}"></script>

<!-- Custom scripts for this template -->
<script src="{{asset('assets/js/resume.min.js')}}"></script>

</body>

</html>
